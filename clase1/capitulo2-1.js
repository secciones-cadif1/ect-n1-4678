const process = require('node:process');

let nombreEmpleados = ["Jose Rojas","Maria Perez","Jesus Dellan","Luis Torrealba"]
console.log(`El sistema operativo es ${process.platform}`)
console.log(`La versión de node es ${process.version}`)

for (i=0;i<4;i++){
    console.log(`=================TRABAJADOR ${i+1}==========================`)
    let sueldo = Math.round(Math.random()*(500-200)+200)
    let añosServicio = Math.round(Math.random()*10)
    let bono = 0;

    console.log(`El trabajador ${nombreEmpleados[i].toUpperCase()} tiene un sueldo base de ${sueldo}$`)
    console.log(`Los años de servicio del trabajador son ${añosServicio} años`)

    if (añosServicio > 5)
        bono = Math.round(sueldo*0.1);
    else
        console.log("No le toca bonificación")

    console.log(`Le toca bonificación que es de ${bono}$`)
    sueldoFinal = sueldo + bono;
    console.log(`El sueldo final es ${sueldoFinal}$`)
}