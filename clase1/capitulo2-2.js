const process = require('process');
const os = require("os")

console.log(`La cantidad de parámetros recibidos es ${process.argv.length-2}`)
console.log(`El directorio del usuario actual es ${os.homedir()}`)

cpus = os.cpus();

console.log(`La velocidad del CPU es ${cpus[0].speed/1000} Ghz`)

if (process.argv.length > 2){
    console.log(`Los parámetros que esta recibiendo son:`)
    for (i=2;i<process.argv.length;i++)
        console.log(process.argv[i])
}
