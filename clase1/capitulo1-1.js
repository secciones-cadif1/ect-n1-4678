
let nombreEmpleado = "Jose Rojas"
let sueldo = Math.round(Math.random()*(500-200)+200)
let añosServicio = Math.round(Math.random()*10)
let bono = 0;

console.log(`El trabajador ${nombreEmpleado.toUpperCase()} tiene un sueldo base de ${sueldo}$`)
console.log(`Los años de servicio del trabajador son ${añosServicio} años`)

if (añosServicio > 5)
    bono = Math.round(sueldo*0.1);
else
    console.log("No le toca bonificación")

console.log(`Le toca bonificación que es de ${bono}$`)
sueldoFinal = sueldo + bono;
console.log(`El sueldo final es ${sueldoFinal}$`)