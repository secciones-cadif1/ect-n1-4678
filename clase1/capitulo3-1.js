var ProgressBar = require('progress');
 
console.log("Procesando, espere por favor ...")
var bar = new ProgressBar(':bar', { curr :50, complete :"#", incomplete :"=",total: 100 });
var timer = setInterval(function () {
  bar.tick();
  if (bar.complete) {
    console.log('\Se ha completado el procesamiento\n');
    clearInterval(timer);
  }
}, 100);