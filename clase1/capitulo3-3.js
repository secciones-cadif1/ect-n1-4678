const readLineSync = require("readline-sync")
const process = require("process")

let interactivo = false
let nombreParametro

if (process.argv.length > 2){
    parametro = process.argv[2];
    if (parametro == "i")
        interactivo = true

    if (process.argv.length > 3)
        nombreParametro = process.argv[3]
}

let nombreEmpleado;
let sueldo ;
let añosServicio;

if (interactivo){
    nombreEmpleado = readLineSync.question('Introduzca el nombre: ');
    sueldo = readLineSync.question('Introduzca el sueldo: ');
    añosServicio = readLineSync.question('Introduzca los años de servicio: ');
}else{
    nombreEmpleado = nombreParametro!=undefined?nombreParametro:"Jose Rojas"
    sueldo = Math.round(Math.random()*(500-200)+200)
    añosServicio = Math.round(Math.random()*10)
}

let bono = 0;

console.log(`El trabajador ${nombreEmpleado.toUpperCase()} tiene un sueldo base de ${sueldo}$`)
console.log(`Los años de servicio del trabajador son ${añosServicio} años`)

if (añosServicio > 5){
    bono = Math.round(sueldo*0.1);
    console.log(`Le toca bonificación que es de ${bono}$`)
}else
    console.log("No le toca bonificación")

sueldoFinal = sueldo + bono;
console.log(`El sueldo final es ${sueldoFinal}$`)